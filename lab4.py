import sys
import pytest


def treugolnik(num):
    count = 0
    ob = []
    for i in range(1, num + 1 ):
        for j in range(1, num + 1 ):
            for k in range(1, num + 1 ):
                if i == j == k or i + j + k != num:
                    continue
                else:
                    if i + j + k == num and i == j and i + j > k:
                        dop = [i, j, k]
                        ob.append(dop)
                        count += 1
    return count, ob


def test_1():
    assert treugolnik(21) == (4, [[6, 6, 9], [8, 8, 5], [9, 9, 3], [10, 10, 1]])


def test_2():
    assert treugolnik(15) == (3, [[4, 4, 7], [6, 6, 3], [7, 7, 1]])


def test_3():
    assert treugolnik(12) == (1, [[5, 5, 2]])


def test_4():
    assert treugolnik(22) == (5, [[6, 6, 10], [7, 7, 8], [8, 8, 6], [9, 9, 4], [10, 10, 2]])


def test_5():
    assert treugolnik(13) == (3, [[4, 4, 5], [5, 5, 3], [6, 6, 1]])



def check_input(num):
    while True:
        try:
            num = int(num)
            if num > 0:
                return num
            else:
                raise ValueError("Вы ввели не натуральное число." )
        except ValueError:
            raise ValueError("Вы ввели некорректные значения." )


def main():
    while True:
        try:
            num = str(input("Введите натуральное число: " ))
            num = check_input(num)
            var, stor = treugolnik(num)
            print("Вариантов получения треугольника: ", var)
            for i in stor:
                print("Стороны треугольника: ", i )
            break
        except ValueError:
            print("Вы ввели некорректные значения!!! Повторите попытку" )


if __name__ == '__main__':
    main()